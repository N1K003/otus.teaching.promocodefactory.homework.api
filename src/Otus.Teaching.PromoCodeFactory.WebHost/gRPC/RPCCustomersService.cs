﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.gRPC
{
    public class RpcCustomersService : RPCCustomersService.RPCCustomersServiceBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public RpcCustomersService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<RPCCustomerResponse> GetCustomer(RPCUUIDRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(request.GetGuid());

            var result = customer.ToRpcCustomerResponse();

            return result;
        }


        public override async Task GetCustomers(RPCEmptyRequest request, IServerStreamWriter<RPCCustomerShortResponse> responseStream,
            ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var tasks = customers.Select(x => responseStream.WriteAsync(x.ToRpcCustomerShortResponse()));

            await Task.WhenAll(tasks);
        }

        public override async Task<RPCCustomerResponse> CreateCustomer(RPCCreateCustomerRequest request, ServerCallContext context)
        {
            var preferencesIds = request.Preferences.Select(x => x.GetGuid()).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesIds);

            var customer = request.MapToCustomer(preferences);

            await _customerRepository.AddAsync(customer);

            return customer.ToRpcCustomerResponse();
        }

        public override async Task<RPCStatusResponse> DeleteCustomer(RPCUUIDRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(request.GetGuid());

            if (customer == null)
            {
                return new RPCStatusResponse {Status = StringResponseConstants.EntryNotFound};
            }

            await _customerRepository.DeleteAsync(customer);

            return new RPCStatusResponse {Status = StringResponseConstants.Ok};
        }

        public override async Task<RPCStatusResponse> EditCustomer(RPCEditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(request.GetId());

            if (customer == null)
            {
                return new RPCStatusResponse {Status = StringResponseConstants.EntryNotFound};
            }

            var preferencesIds = request.Preferences.Select(x => x.GetGuid()).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesIds);

            customer = request.MapToCustomer(preferences);

            await _customerRepository.UpdateAsync(customer);

            return new RPCStatusResponse {Status = StringResponseConstants.Ok};
        }

        private static class StringResponseConstants
        {
            public const string EntryNotFound = "EntryNotFound";
            public const string Ok = "Ok";
        }
    }

    internal static class RpcMessageExtensions
    {
        public static Guid GetGuid(this RPCUUIDRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Value))
            {
                throw new ArgumentNullException(nameof(request.Value));
            }

            return Guid.Parse(request.Value);
        }

        public static Guid GetId(this RPCEditCustomerRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Id))
            {
                throw new ArgumentNullException(nameof(request.Id));
            }

            return Guid.Parse(request.Id);
        }

        public static RPCCustomerResponse ToRpcCustomerResponse(this Customer customer)
        {
            var result = new RPCCustomerResponse
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };

            if (customer.Preferences != null && customer.Preferences.Any())
            {
                result.Preferences.AddRange(customer.Preferences.Select(x => new RPCCustomerPreferenceResponse
                {
                    Id = x.PreferenceId.ToString(),
                    Name = x.Preference.Name
                }));
            }

            return result;
        }

        public static RPCCustomerShortResponse ToRpcCustomerShortResponse(this Customer customer)
        {
            return new RPCCustomerShortResponse
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }

        public static Customer MapToCustomer(this RPCCreateCustomerRequest request, IEnumerable<Preference> preferences)
        {
            var id = Guid.NewGuid();

            return new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference
                {
                    CustomerId = id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };
        }

        public static Customer MapToCustomer(this RPCEditCustomerRequest request, IEnumerable<Preference> preferences)
        {
            var id = request.GetId();

            return new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference
                {
                    CustomerId = id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };
        }
    }
}